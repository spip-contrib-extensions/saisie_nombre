<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insérer automatiquement l'appel au js dans les formulaires
 * @param array $flux
 * @return array $flux modifié
 **/
function saisie_nombre_formulaire_fond($flux) {
	static $flag;
	if (!$flag and stripos($flux['data'], 'autonumeric-decimalPlaces')!==false) {
		$flux['data'] .= "<script type='text/javascript' src='".timestamp(find_in_path('javascript/autoNumeric.min.js'))."'></script>";
		$flux['data'] .= "<script type='text/javascript' src='".timestamp(find_in_path('javascript/saisie_nombre.js'))."'></script>";
		$flag = true;
	}
	return $flux;
}
