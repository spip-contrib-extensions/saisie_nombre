<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/saisie_nombre.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'option_currencySymbolPlacement_label' => 'Emplacement de la devise (ou autre unité)',
	'option_currencySymbolPlacement_p_label' => 'En préfixe',
	'option_currencySymbolPlacement_s_label' => 'En suffixe',
	'option_currencySymbol_label' => 'Devise (ou autre unité)',
	'option_currency_label' => 'Afficher une devise (ou autre unité)',
	'option_decimalCharacter_label' => 'Séparateur de décimales',
	'option_decimalPlaces_label' => 'Nombre de décimales à afficher',
	'option_digitGroupSeparator_label' => 'Séparateur des milliers',
	'option_predefinedOption_Brazilian_label' => 'Brésil',
	'option_predefinedOption_British_label' => 'Royaumes-Unis',
	'option_predefinedOption_Chinese_label' => 'Chine',
	'option_predefinedOption_French_label' => 'France',
	'option_predefinedOption_Japanese_label' => 'Japon',
	'option_predefinedOption_None_label' => 'Aucune',
	'option_predefinedOption_NorthAmerican_label' => 'États-Unis',
	'option_predefinedOption_Spanish_label' => 'Espagne',
	'option_predefinedOption_Swiss_label' => 'Suisse',
	'option_predefinedOption_Turkish_label' => 'Turkish',
	'option_predefinedOption_label' => 'Option prédéfinie',

	// S
	'saisie_nombre_description' => 'Un nombre,  entier ou décimal.',
	'saisie_nombre_titre' => 'Nombre'
);
