<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/saisie_nombre.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'saisie_nombre_description' => 'Une saisie pour manipuler des nombres, entiers ou décimaux.',
	'saisie_nombre_slogan' => 'Des nombres formatables '
);
