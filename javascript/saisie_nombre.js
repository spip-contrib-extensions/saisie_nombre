// Petit hack pour être sur d'envoyer la forme normalisée, quelque soit les réglages ARIA
saisies_nombre={};
function saisie_nombre_sync_hidden($saisie_nombre) {
	id = $saisie_nombre.attr('id');
	val =  saisies_nombre[id].getNumericString();
	$('#'+id+'_hidden').val(val);
};
// Configuration d'un champ
function saisie_nombre_autonumeric($champ) {
	id = $champ.attr('id');
	options = {
		decimalCharacter : $champ.attr('data-autonumeric-decimalCharacter'),
		digitGroupSeparator : $champ.attr('data-autonumeric-digitGroupSeparator'),
		decimalPlaces : $champ.attr('data-autonumeric-decimalPlaces')
	};
	if (id in saisies_nombre) {
		if (AutoNumeric.areSettingsValid(options)) {
			saisies_nombre[id].options.reset();
			saisies_nombre[id].update(options);
		} else {
			console.warn('Options de la saisie nombre invalides. Pas de modification des options actuelles.');
		}
	} else {
		try {
			saisies_nombre[id] = new AutoNumeric('#'+id , options);
		} catch {
			console.warn('Options de la saisie nombre invalides. Bascule vers les options par défaut d\'AutoNumeric');
			saisies_nombre[id] = new AutoNumeric('#'+id);
		}
	}
}
// Mise en place du code proprement dit
$(function(){
	$('input.nombre').each(function() {
		name = $(this).attr('name');
		id = $(this).attr('id');
		after = '<input type="hidden" name="' + name + '" id="' + id + '_hidden" value=""/>';
		saisie_nombre_autonumeric($(this));
		$(this).after(
			after
		).change(function (){
			saisie_nombre_sync_hidden($(this));
		});
		saisie_nombre_sync_hidden($(this));
	});
});
